# Setup Automation
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time
irm get.scoop.sh | iex
scoop install git
scoop bucket add main
scoop bucket add extras
scoop bucket add versions
scoop bucket add nonportable

# Compiler/Platforms
scoop install python38
scoop install python39
scoop install python310
scoop install python311
scoop install dotnet-sdk
scoop install vcredist2022
scoop uninstall vcredist2022

# Code Editors
scoop install vscode
scoop install vim
# scoop install godot

# Virtualization / Containers
scoop install docker
scoop install virtualbox-with-extension-pack-np