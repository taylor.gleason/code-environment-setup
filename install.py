import pip
import sys

_all_ = ["os",
"requests"]

windows = []

linux = []

darwin = []

def install(packages):
    for package in packages:
        pip.main(['install', package])

if __name__ == '__main__':
    print("Run: Install")
    print(sys.platform)

    install(_all_) 
    if sys.platform.startswith('win'):
        install(windows)
    if sys.platform.startswith('linux'):
        install(linux)
    if sys.platform.startswith == 'darwin': # MacOS
        install(darwin)